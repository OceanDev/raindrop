const fetch = require('node-fetch')
const JSDOM = require('jsdom').JSDOM
const formurlencoded = require('form-urlencoded').default
const mongo = require('mongodb');

var client = mongo.MongoClient;
var mongouri = process.env.mongouri

const cookie = process.env.cookie

const imposterWebhook = process.env.imposterWebhook
const humanWebhook = process.env.humanWebhook

const collectionName = 'answers2'

function getCSRF(document) {
	const csrf = document.getElementsByTagName('gremlin-app')[0].getAttribute('csrf')
	return csrf
}
function updateCSRF(document) {
	const csrf = getCSRF(document)
	global.csrf = csrf
}

const BASEURL = 'https://gremlins-api.reddit.com'

async function doWebhook(text, isCorrect) {
	if (!imposterWebhook) return
	var webhookUrl
	if (isCorrect)
		webhookUrl = imposterWebhook
	else
		webhookUrl = humanWebhook
	r = await fetch(webhookUrl, {
		method: 'POST',
		body: JSON.stringify({
			content: '> ' +  text
		}),
		headers: {
			'content-type': 'application/json'
		}
	})
	console.log(await r.text())
}

async function getQuestions() {
	const r = await fetch(
		`${BASEURL}/room`, {
			headers: {cookie: cookie}
		}
	)
	const html = await r.text()
	const dom = new JSDOM(html)
	const document = dom.window.document

	updateCSRF(document)

	possibleAnswers = []

	const answerEls = document.getElementsByTagName('gremlin-note') // array of questions
	for (const answerEl of answerEls) {
		const answerId = answerEl.id
		const answerText = answerEl.textContent.trim()
		possibleAnswers.push({
			'id': answerId,
			'text': answerText
		})
	}
	return possibleAnswers
}

async function postAnswer(noteId) {
	const postBody = formurlencoded({
		'undefined': 'undefined', // no idea why this is here
		note_id: noteId,
		csrf_token: global.csrf
	})
	const r = await fetch(
		`${BASEURL}/submit_guess`, {
			headers: {
				cookie: cookie,
				'content-type': 'application/x-www-form-urlencoded'
			},
			method: 'POST',
			body: postBody
		}
	)
	const textResp = await r.text()
	const data = JSON.parse(textResp)
	const isCorrect = data.result === 'WIN'

	if (isCorrect)
		global.totalCorrect++
	else
		global.totalIncorrect++

	return isCorrect
}

async function insertAnswer(text, isCorrect) {
	global.answersColl.updateOne({
		'text': text
	}, {
		'$set': {
			'text': text,
			'is_correct': isCorrect
		}
	}, {
		'upsert': true
	})
}

global.totalCorrect = 0
global.totalIncorrect = 0

function testIsCorrect(text) {
	return new Promise((resolve) => {
		global.answersColl.findOne({
			text: text
		}, function(err, document) {
			if (document === null) return resolve(null)
			const isCorrect = document.is_correct
			resolve(isCorrect)
		})
	})
}

async function doQuestion() {
	try {
		var questions = await getQuestions()
	} catch {
		console.log('Error while getting questions. Waiting a second and trying again')
		await sleep(1000)
		return await doQuestion()
	}

	var foundCorrect = false

	var possibleAnswers = []
	var correctAnswer = {}
	var isNew = false
	var question

	for (var question of questions) {
		var isCorrect = await testIsCorrect(question['text'])
		if (isCorrect) {
			foundCorrect = true
			correctAnswer = question
		}
		if (isCorrect === null)
			possibleAnswers.push(question)
	}
	if (possibleAnswers.length == 0 && !foundCorrect) {
		console.log('No possible answers found')
		question = questions[0]
	}
	else if (!foundCorrect) {
		question = possibleAnswers[0]
		isNew = true
	} else {
		question = correctAnswer
	}

	try {
		var isCorrect = await postAnswer(question['id'])
	} catch {
		console.log('Error posting answer')
		await sleep(1000)
		return
	}

	if (foundCorrect) {
		if (isCorrect)
			console.log('Found correct answer that I already knew about')
		else if (isCorrect === false)
			console.log('Uh oh, found answer that db said was incorrect but actually was:', question['text'])
	} else {
		console.log('is correct:', isCorrect)
		console.log(global.totalCorrect / (global.totalIncorrect + global.totalCorrect) * 100 + '%')
		isNew = true
	}

	await insertAnswer(question['text'], isCorrect)

	if (isNew) {
		doWebhook(question['text'], isCorrect)
	}

	// If it's correct mark the other ones as incorrect
	if (isCorrect) {
		for (const q of questions) {
			if (q['text'] !== question['text']) {
				var isCorrect = await testIsCorrect(q['text'])
				if (isCorrect === true)
					console.log('Uh oh, database said answer was correct but there was another correct answer:', '"' + q['text'] + '",', '"' + question['text'] + '"')
				await insertAnswer(q['text'], false)
			}
		}
	}
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function main(err, db) {
	global.answersColl = db.db('imposter').collection(collectionName)
	// global.answersColl.deleteMany({})

	const ratelimit = 200

	while (true) {
		var startTime = new Date()
		await doQuestion()
		var endTime = new Date()
		console.log(endTime - startTime)
		if (endTime - startTime < ratelimit)
			await sleep(ratelimit - (endTime - startTime))
	}
}

client.connect(mongouri, main)
